import 'package:json_annotation/json_annotation.dart';

part 'user_model.g.dart';
// Need to show Shakeels errors !!

@JsonSerializable()
class UserModel {
  final List<UserModelObj> results;

  UserModel({
    required this.results,
  });

  //fromJson
  factory UserModel.fromJson(Map<String, dynamic> json) =>
      _$UserModelFromJson(json);

  //ToJson
  Map<String, dynamic> toJson() => _$UserModelToJson(this);

  getUserData(String s) {}
}

@JsonSerializable()
class UserModelObj {
  final String gender;
  final UserNameObj name;
  final String email;
  final LoginObj login;

  UserModelObj({
    required this.gender,
    required this.name,
    required this.email,
    required this.login,
  });

  //fromjson
  factory UserModelObj.fromJson(Map<String, dynamic> map) =>
      _$UserModelObjFromJson(map);

  //ToJson
  Map<String, dynamic> toJson() => _$UserModelObjToJson(this);
}

@JsonSerializable()
class UserNameObj {
  final String title;
  final String first;
  final String last;

  UserNameObj({
    required this.title,
    required this.first,
    required this.last,
  });

  factory UserNameObj.fromJson(Map<String, dynamic> map) =>
      _$UserNameObjFromJson(map);

  Map<String, dynamic> toJson() => _$UserNameObjToJson(this);
}

@JsonSerializable()
class LoginObj {
  final String uuid;
  final String username;
  final String password;
  final String salt;
  final String md5;
  final String sha1;
  final String sha256;

  LoginObj({
    required this.uuid,
    required this.username,
    required this.password,
    required this.salt,
    required this.md5,
    required this.sha1,
    required this.sha256,
  });

  factory LoginObj.fromJson(Map<String, dynamic> map) =>
      _$LoginObjFromJson(map);

  Map<String, dynamic> toJson() => _$LoginObjToJson(this);
}
