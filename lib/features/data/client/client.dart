// ignore_for_file: depend_on_referenced_packages

import 'package:dio/dio.dart';
import 'package:notiondemo/features/data/model/user_model.dart';
import 'package:retrofit/retrofit.dart';

part 'client.g.dart';

/// Remote client for interacting with remote server
@RestApi()
abstract class RestClient {
  /// flutter pub run build_runner build --delete-conflicting-outputs
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  /// Request to get and parse the json and returl a list of User Model
  @GET("{baseUrl}")
  Future<UserModel> getUserData(
    @Path() final String baseUrl,
  );
}
