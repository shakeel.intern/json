
// import 'package:flutter/material.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:notiondemo/features/presentation/main_page/widgets/bottom_nav.dart';

// import '../../../data/model/user_model.dart';
// import 'bloc/home_bloc.dart';

// class HomeScreen extends StatefulWidget {
//   const HomeScreen({Key? key}) : super(key: key);

//   @override
//   State<HomeScreen> createState() => _HomeScreenState();
// }

// class _HomeScreenState extends State<HomeScreen> {

//   UserModel? users;
//   bool _isloading = false;

//   @override
//   void initState() {
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     BlocProvider.of<HomeBloc>(context).add(
//       GetUserDataEvent(),
//     );
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text('Lets API'),
//         backgroundColor: Colors.black,
//       ),
//       body: BlocConsumer<HomeBloc, HomeState>(
//         listener: (context, state) {
//           if (state is UserDataLoadedState) {
//             users = state.userModel;
//           }
//           if (state is UserDataLoadingState) {
//             _isloading = true;
//           }
//         },
//         builder: (context, state) {
//           if (users == null) {
//             return const SizedBox(
//               child: Center(
//                 child: Text(
//                   ("emtpy"),
//                 ),
//               ),
//             );
//           }

        
// if (_isloading == true) {
//   return const SizedBox(
//     child: Center(
//       child: CircularProgressIndicator(), // Replace the Text widget with a CircularProgressIndicator
//     ),
//   );  
//           }
//           return ListView.builder(
//             //lists the user data
//             itemCount: users!.results.length,
//             itemBuilder: (context, index) {
//               final user = users!.results[index];
//               final color = user.gender == 'male' ? Colors.blue : Colors.green;
//               return ListTile(
//                 title: Text(user.name.first),
//                 subtitle: Text(user.email),
//                 tileColor: color,
//               );
//             },
//           );
//         },
//       ),
//       bottomNavigationBar: const BottomNavigationWidget(),
//     );
//   }
// }


import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:notiondemo/features/presentation/main_page/widgets/bottom_nav.dart';

import '../../../data/model/user_model.dart';
import 'bloc/home_bloc.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  UserModel? users;
  bool _isloading = false;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<HomeBloc>(context).add(
      GetUserDataEvent(),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Lets API'),
        backgroundColor: Colors.black,
      ),
      body: BlocConsumer<HomeBloc, HomeState>(
        listener: (context, state) {
          if (state is UserDataLoadedState) {
            users = state.userModel;
          }
          if (state is UserDataLoadingState) {
            _isloading = true;
          }
        },
        builder: (context, state) {
          if (users == null) {
            return const SizedBox(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }

          if (_isloading == true) {
            return const SizedBox(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );  
          }

          return ListView.builder(
            itemCount: users!.results.length,
            itemBuilder: (context, index) {
              final user = users!.results[index];
              final color = user.gender == 'male' ? Colors.blue : Colors.green;
              return ListTile(
                title: Text(user.name.first),
                subtitle: Text(user.email),
                tileColor: color,
              );
            },
          );
        },
      ),
      bottomNavigationBar: const BottomNavigationWidget(),
    );
  }
}
