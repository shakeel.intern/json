part of 'home_bloc.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();
}

class GetUserDataEvent extends HomeEvent {
  @override
  List<Object> get props => [];
}


abstract class LoaderEvent {}

class StartLoading extends LoaderEvent {}

class StopLoading extends LoaderEvent {}


