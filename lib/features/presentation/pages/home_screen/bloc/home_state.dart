part of 'home_bloc.dart';

abstract class HomeState extends Equatable {
  const HomeState();
}

class HomeInitial extends HomeState {
  @override
  List<Object> get props => [];
}

class UserDataLoadedState extends HomeState {
  final UserModel userModel;
  const UserDataLoadedState({required this.userModel});
  @override
  List<Object> get props => [
        userModel,
      ];
}



class UserDataLoadingState extends HomeState {
  final UserModel userModel;
  const UserDataLoadingState({required this.userModel});
  @override
  List<Object> get props => [
        userModel,
      ];
}


abstract class LoaderState {}

class LoadingInProgress extends LoaderState {}

class LoadingComplete extends LoaderState {}
