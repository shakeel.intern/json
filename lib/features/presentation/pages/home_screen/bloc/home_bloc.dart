import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:notiondemo/features/data/model/user_model.dart';

import '../../../../data/client/client.dart';

part 'home_event.dart';
part 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeBloc() : super(HomeInitial()) {
    on<GetUserDataEvent>((event, emit) async => getUserData(event, emit));
  }

  getUserData(
    GetUserDataEvent event,
    Emitter<HomeState> emit,
  ) async {
    final dio = Dio();
    final client = RestClient(dio);
    UserModel response =
        await client.getUserData('https://randomuser.me/api/?results=30');
    emit(
      UserDataLoadedState(
        userModel: response,
      ),
    );
  }
}

// class LoaderBloc extends Bloc<LoaderEvent, LoaderState> {
//   LoaderBloc() : super(LoadingComplete());

//   @override
//   Stream<LoaderState> mapEventToState(LoaderEvent event) async* {
//     if (event is StartLoading) {
//       yield LoadingInProgress();
//     } else if (event is StopLoading) {
//       yield LoadingComplete();
//     }
//   }
// }


