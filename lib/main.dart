import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:notiondemo/features/presentation/pages/home_screen/bloc/home_bloc.dart';
import 'package:notiondemo/features/presentation/pages/home_screen/home_screen.dart';

void main() async {
  runApp(
    MaterialApp( 
      theme: ThemeData(
        primarySwatch: Colors.blue,
        // textTheme: TextTheme(
        //   bodyText1: TextStyle(color: Colors.white),
        //   bodyText2: TextStyle(color: Colors.white),
        // )
      ),
      home: BlocProvider<HomeBloc>(
        create: (context) => HomeBloc(),
        child: const HomeScreen(),
      ),
    ),
  );
}
